#define MY_DEBUG 
#define MY_GATEWAY_SERIAL
#include <MySensors.h>
#include "DHT.h"

//#define DHTTYPE DHT11   // DHT 11 
#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

DHT *sensors[24];
int sensor_pins[24] = {23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 22, 24, 26, 28, 30, 32, 34, 36};
byte sensor_hum_ids[24] = {0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46};
byte sensor_temp_ids[24] = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47};
float sensor_hum_prev[24];
float sensor_temp_prev[24];

void setup() 
{
    Serial1.begin(9600);
    for(byte i=0; i<24; i++){
      sensors[i] = new DHT(sensor_pins[i], DHTTYPE);
      sensors[i]->begin();
    }
}

void presentation()
{
  sendSketchInfo("Sensors", "1.0");
  for (byte i=0; i<24; i++){
    present(sensor_temp_ids[i], S_TEMP);
    present(sensor_hum_ids[i], S_HUM);
  }
}

void loop() 
{
  for (byte i=0; i<24; i++){
    float sensor_data[2];
    sensor_data[0] = sensors[i]->readHumidity();
    sensor_data[1] = sensors[i]->readTemperature();
    if (sensor_hum_prev[i] != sensor_data[0]){
      send_humidity(sensor_data[0], i);
      sensor_hum_prev[i] = sensor_data[1];
    }
    if (sensor_temp_prev[i] != sensor_data[0]){
      send_temperature(sensor_data[1], i);
      sensor_temp_prev[i] = sensor_data[0];
    }
  }  
}

void send_temperature(float t, byte id){
      if (isnan(t)) 
    {
        //Serial.println("Failed to read from DHT");
    } 
    else 
    {
        MyMessage msg(sensor_temp_ids[id], V_TEMP);
        if (t != 0.0) send(msg.set(t,1));
    }
}

void send_humidity(float h, byte id){
    
      if (isnan(h)) 
    {
        //Serial.println("Failed to read from DHT");
    } 
    else 
    {
        MyMessage msg(sensor_hum_ids[id], V_HUM);
        if (h != 0.0) send(msg.set(h,1));
        
    }
}

void receive(const MyMessage &message) {
}
