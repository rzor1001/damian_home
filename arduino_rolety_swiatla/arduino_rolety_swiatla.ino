#define MY_DEBUG 
#define MY_GATEWAY_SERIAL
#include <MySensors.h>
#include <EEPROM.h>
#include <Wire.h>
#include <TimerOne.h>
#include "Adafruit_MCP23017.h"

#define cover_timeout 120 //czas po ktorym nastepuje wylaczenie pinu sterowania roleta (w sekundach)
#define eeprom_reset_pin A0
boolean light_switch_statuses[24];
byte cover_switch_statuses[24];
byte cover_timers[24];
byte cover_statuses[24]; //1 - run_up, 2 - run_down, 3 - up, 4 - down, 5 - stopped_up, 6 - stopped_down
byte cover_out[24];
byte light_ids[24] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24};
int cover_ids[24] = {51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74};
byte light_switch_pins[24] = {37, 35, 33, 31, 29, 27, 25, 23, 62, 63, 54, 65, 66, 67, 68, 69, 11, 10, 9, 8, 7, 6, 5, 4};
byte light_pins[24] = {52, 50, 48, 46, 44, 42, 40, 38, 36, 34, 32, 30, 28, 26, 24, 22, 53, 51, 49, 47, 45, 43, 41, 39};
byte cover_pins_1[24][2] = {{0,1}, {2, 3}, {4, 5}, {6, 7}, {8, 9}, {10, 11}, {12, 13}, {14, 15}};
byte cover_pins_2[24][2] = {{0,1}, {2, 3}, {4, 5}, {6, 7}, {8, 9}, {10, 11}, {12, 13}, {14, 15}};
byte cover_pins_3[24][2] = {{0,1}, {2, 3}, {4, 5}, {6, 7}, {8, 9}, {10, 11}, {12, 13}, {14, 15}};

Adafruit_MCP23017 mcp1;
Adafruit_MCP23017 mcp2;
Adafruit_MCP23017 mcp3;
Adafruit_MCP23017 mcp4;
Adafruit_MCP23017 mcp5;
Adafruit_MCP23017 mcp6;

void setup()
{
  mcp1.begin(0);
  mcp2.begin(1);
  mcp3.begin(2);
  mcp4.begin(3);
  mcp5.begin(4);
  mcp6.begin(5);
    
  for (int i=0;i<16;i++) {
    mcp1.pinMode(i, OUTPUT);
    mcp2.pinMode(i, OUTPUT);
    mcp3.pinMode(i, OUTPUT);
    mcp1.digitalWrite(i, LOW);
    mcp2.digitalWrite(i, LOW);
    mcp3.digitalWrite(i, LOW);
    mcp4.pinMode(i, INPUT);
    mcp4.pullUp(i, HIGH);
    mcp5.pinMode(i, INPUT);
    mcp5.pullUp(i, HIGH);
    mcp6.pinMode(i, INPUT);
    mcp6.pullUp(i, HIGH);
  }
 
  pinMode(eeprom_reset_pin, INPUT_PULLUP);
  if (!digitalRead(eeprom_reset_pin)) reset_eeprom();
  
	for(byte i=0; i<24; i++){
    pinMode(light_switch_pins[i], INPUT_PULLUP);
    pinMode(light_pins[i], OUTPUT);
    digitalWrite(light_pins[i], EEPROM.read(light_ids[i]));
    light_switch_statuses[i] = false;
    cover_switch_statuses[i] = false;
    cover_timers[i] = 0;
    cover_statuses[i] = EEPROM.read(cover_ids[i]);
    cover_out[i] = 0;
	}
  
  Timer1.initialize(1000000); // set a timer of length 100000 microseconds (or 0.1 sec - or 10Hz => the led will blink 5 times, 5 cycles of on-and-off, per second)
  Timer1.attachInterrupt( timer_isr ); // attach the service routine here
}

void presentation()
{
	sendSketchInfo("Relays", "1.0");
  for (byte i=0; i<24; i++){
    present(light_ids[i], S_LIGHT);
    present(cover_ids[i], S_LIGHT);
  }
  send_cover_statuses();
  send_light_statuses();
}

void loop()
{
	for(byte i=0; i<24; i++){
	  if (!digitalRead(light_switch_pins[i]) && !light_switch_statuses[i]){
      digitalWrite(light_pins[i], !digitalRead(light_pins[i]));
      EEPROM.write(light_ids[i], digitalRead(light_pins[i]));
      light_switch_statuses[i] = true;
      MyMessage msg(light_ids[i], V_LIGHT);
      send(msg.set(digitalRead(light_pins[i])));
      delay(50);
	  }
    else if (digitalRead(light_switch_pins[i]) && light_switch_statuses[i]){
      light_switch_statuses[i] = false;
      delay(50);
    }
	}
 
 byte tmp_state[24]; //0 - none, 1 - down, 2 - up
 for(byte j=0; j<3; j++){  
   for(byte i=0; i<8; i++){
    byte tmp_i = i * 2;
    if (!mcp4.digitalRead(tmp_i)){
      tmp_state[i] = 1;
    }
    else if (!mcp4.digitalRead(tmp_i+1)){
      tmp_state[i] = 2;
    }
    else{
      tmp_state[i] = 0;
    }
    
    if (!mcp5.digitalRead(tmp_i)){
      tmp_state[i+8] = 1;
    }
    else if (!mcp5.digitalRead(tmp_i+1)){
      tmp_state[i+8] = 2;
    }
    else{
      tmp_state[i+8] = 0;
    }
    
    if (!mcp6.digitalRead(tmp_i)){
      tmp_state[i+16] = 1;
    }
    else if (!mcp6.digitalRead(tmp_i+1)){
      tmp_state[i+16] = 2;
    }
    else{
      tmp_state[i+16] = 0;
    }
  }
 }

 for (byte i=0; i<24; i++){
    if(tmp_state[i] != cover_switch_statuses[i]){
      if(tmp_state[i] == 1){
        if (cover_statuses[i] == 1 || cover_statuses[i] == 2){
          cover_statuses[i] = 3;
          cover_out[i]=0;
          cover_timers[i] = cover_timeout;
        }
        else cover_statuses[i] = 2;
        delay(50);
        cover_switch_statuses[i] = 1;
        cover_timers[i] = 0;
        EEPROM.write(cover_ids[i], cover_statuses[i]);
        send_cover_statuses();
        send_light_statuses();
      }
      else if(tmp_state[i] == 2){
        if (cover_statuses[i] == 1 || cover_statuses[i] == 2){
          cover_statuses[i] = 4;
          cover_out[i]=0;
          cover_timers[i] = cover_timeout;
        }
        else cover_statuses[i] = 1;
        delay(50);
        cover_switch_statuses[i] = 2;
        cover_timers[i] = 0;
        EEPROM.write(cover_ids[i], cover_statuses[i]);
        send_cover_statuses();
        send_light_statuses();
      }
      else{
        delay(50);
        cover_switch_statuses[i] = 0;
      }
    }
 }
 

 for (byte i=0; i<24; i++){
  cover_control(cover_out[i], i);
 }
}


void receive(const MyMessage &message) {
  if (message.type==V_LIGHT) {
    byte lamp_number = 100;
    byte cover_number = 100;

    for (byte i=0;i<24;i++){
      if (light_ids[i] == message.sensor) {
        lamp_number = i;
        break;
      }
    }

    for (byte i=0;i<24;i++){
      if (cover_ids[i] == message.sensor){
        cover_number = i;
        break;
      }
    }

    if (lamp_number != 100){
      digitalWrite(light_pins[lamp_number], message.getBool());
      EEPROM.write(light_ids[lamp_number], message.getBool());
    }
    
    if (cover_number != 100){
      if (message.getBool() == 0) {
        if (cover_statuses[cover_number] == 1 || cover_statuses[cover_number] == 2){
          cover_statuses[cover_number] = 3;
          cover_out[cover_number]=0;
          cover_timers[cover_number] = cover_timeout;
        }
        else cover_statuses[cover_number] = 2;
      }
      else {
        if (cover_statuses[cover_number] == 1 || cover_statuses[cover_number] == 2){
          cover_statuses[cover_number] = 4;
          cover_out[cover_number]=0;
          cover_timers[cover_number] = cover_timeout;
        }
        else cover_statuses[cover_number] = 1;
      }
      cover_timers[cover_number] = 0;
      EEPROM.write(cover_ids[cover_number], cover_statuses[cover_number]);
    }
    send_cover_statuses();
    send_light_statuses();
   } 
}

void reset_eeprom(){
  for(byte i=0;i<24;i++){
    EEPROM.write(light_ids[i], false);
    EEPROM.write(cover_ids[i], 1);
  }
}

void send_light_statuses(){
  for(byte i=0; i<24; i++){
    MyMessage msg(light_ids[i], V_LIGHT);
    send(msg.set(digitalRead(light_pins[i])));
  }
}

void send_cover_statuses(){
  for(byte i=0; i<24; i++){
    MyMessage msg(cover_ids[i], V_LIGHT);
    if (cover_statuses[i] == 2 || cover_statuses[i] == 4 || cover_statuses[i] == 6) send(msg.set(0));
    else if (cover_statuses[i] == 1 || cover_statuses[i] == 3 || cover_statuses[i] == 5) send(msg.set(1));
  }
}

void cover_control(byte cover_status, byte id){
  //0 - up = 0, down = 0  stop
  //1 - up = 1, down = 0  up
  //2 - up = 0, down = 1  down
  if (id < 8){
    if (cover_status == 0){
      mcp1.digitalWrite(cover_pins_1[id][0], LOW);
      mcp1.digitalWrite(cover_pins_1[id][1], LOW);
    }
    else if (cover_status == 1){
      mcp1.digitalWrite(cover_pins_1[id][0], HIGH);
      mcp1.digitalWrite(cover_pins_1[id][1], LOW);
    }
    else if (cover_status == 2){
      mcp1.digitalWrite(cover_pins_1[id][0], LOW);
      mcp1.digitalWrite(cover_pins_1[id][1], HIGH);
    }
  }
  else if (id >= 8 && id < 16){
      if (cover_status == 0){
        mcp2.digitalWrite(cover_pins_3[id-8][0], LOW);
        mcp2.digitalWrite(cover_pins_3[id-8][1], LOW);
      }
      else if (cover_status == 1){
        mcp2.digitalWrite(cover_pins_3[id-8][0], HIGH);
        mcp2.digitalWrite(cover_pins_3[id-8][1], LOW);
      }
      else if (cover_status == 2){
        mcp2.digitalWrite(cover_pins_3[id-8][0], LOW);
        mcp2.digitalWrite(cover_pins_3[id-8][1], HIGH);
      }
  
  }
  else if (id >= 16 && id < 24){
      if (cover_status == 0){
        mcp3.digitalWrite(cover_pins_3[id-16][0], LOW);
        mcp3.digitalWrite(cover_pins_3[id-16][1], LOW);
      }
      else if (cover_status == 1){
        mcp3.digitalWrite(cover_pins_3[id-16][0], HIGH);
        mcp3.digitalWrite(cover_pins_3[id-16][1], LOW);
      }
      else if (cover_status == 2){
        mcp3.digitalWrite(cover_pins_3[id-16][0], LOW);
        mcp3.digitalWrite(cover_pins_3[id-16][1], HIGH);
      }
  }
}

void timer_isr()
{
    for (byte i=0; i<24; i++){
      if (cover_timers[i] < cover_timeout) {
        if (cover_statuses[i] == 1) cover_out[i]=2;
        else if (cover_statuses[i] == 2) cover_out[i]=1;
        cover_timers[i]++;
      }
      else if (cover_timers[i] >= cover_timeout) {
        if (cover_statuses[i] == 1 || cover_statuses[i] == 5) {
          cover_statuses[i] = 3;
          EEPROM.write(cover_ids[i], cover_statuses[i]);
          cover_out[i]=0;
        }
        else if (cover_statuses[i] == 2 || cover_statuses[i] == 6) {
          cover_statuses[i] = 4;
          EEPROM.write(cover_ids[i], cover_statuses[i]);
          cover_out[i]=0;
        }
      }
    }
}
